\documentclass[]{book}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\hypersetup{unicode=true,
            pdftitle={Statistical Rethinking \textbar{} Notes and Examples},
            pdfauthor={Richard Bownes},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{natbib}
\bibliographystyle{plainnat}
\usepackage{color}
\usepackage{fancyvrb}
\newcommand{\VerbBar}{|}
\newcommand{\VERB}{\Verb[commandchars=\\\{\}]}
\DefineVerbatimEnvironment{Highlighting}{Verbatim}{commandchars=\\\{\}}
% Add ',fontsize=\small' for more characters per line
\usepackage{framed}
\definecolor{shadecolor}{RGB}{248,248,248}
\newenvironment{Shaded}{\begin{snugshade}}{\end{snugshade}}
\newcommand{\AlertTok}[1]{\textcolor[rgb]{0.94,0.16,0.16}{#1}}
\newcommand{\AnnotationTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\AttributeTok}[1]{\textcolor[rgb]{0.77,0.63,0.00}{#1}}
\newcommand{\BaseNTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{#1}}
\newcommand{\BuiltInTok}[1]{#1}
\newcommand{\CharTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\CommentTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textit{#1}}}
\newcommand{\CommentVarTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\ConstantTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\ControlFlowTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{\textbf{#1}}}
\newcommand{\DataTypeTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{#1}}
\newcommand{\DecValTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{#1}}
\newcommand{\DocumentationTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\ErrorTok}[1]{\textcolor[rgb]{0.64,0.00,0.00}{\textbf{#1}}}
\newcommand{\ExtensionTok}[1]{#1}
\newcommand{\FloatTok}[1]{\textcolor[rgb]{0.00,0.00,0.81}{#1}}
\newcommand{\FunctionTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\ImportTok}[1]{#1}
\newcommand{\InformationTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\newcommand{\KeywordTok}[1]{\textcolor[rgb]{0.13,0.29,0.53}{\textbf{#1}}}
\newcommand{\NormalTok}[1]{#1}
\newcommand{\OperatorTok}[1]{\textcolor[rgb]{0.81,0.36,0.00}{\textbf{#1}}}
\newcommand{\OtherTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{#1}}
\newcommand{\PreprocessorTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textit{#1}}}
\newcommand{\RegionMarkerTok}[1]{#1}
\newcommand{\SpecialCharTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\SpecialStringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\StringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\VariableTok}[1]{\textcolor[rgb]{0.00,0.00,0.00}{#1}}
\newcommand{\VerbatimStringTok}[1]{\textcolor[rgb]{0.31,0.60,0.02}{#1}}
\newcommand{\WarningTok}[1]{\textcolor[rgb]{0.56,0.35,0.01}{\textbf{\textit{#1}}}}
\usepackage{longtable,booktabs}
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{5}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}

% Create subtitle command for use in maketitle
\providecommand{\subtitle}[1]{
  \posttitle{
    \begin{center}\large#1\end{center}
    }
}

\setlength{\droptitle}{-2em}

  \title{\emph{Statistical Rethinking} \textbar{} Notes and Examples}
    \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
  \subtitle{Learning from Failure}
  \author{Richard Bownes}
    \preauthor{\centering\large\emph}
  \postauthor{\par}
      \predate{\centering\large\emph}
  \postdate{\par}
    \date{2019-05-02}

\usepackage{booktabs}
\usepackage{amsthm}
\makeatletter
\def\thm@space@setup{%
  \thm@preskip=8pt plus 2pt minus 4pt
  \thm@postskip=\thm@preskip
}
\makeatother

\begin{document}
\maketitle

{
\setcounter{tocdepth}{1}
\tableofcontents
}
\hypertarget{statistical-abridging}{%
\chapter*{Statistical Abridging}\label{statistical-abridging}}
\addcontentsline{toc}{chapter}{Statistical Abridging}

Richard McElreath's \href{http://xcelab.net/rm/statistical-rethinking/}{\emph{Statistical Rethinking} text} is the entry-level textbook for applied researchers. \href{https://www.youtube.com/channel/UCNJK6_DZvcMqNSzQdEkzvzA/playlists}{Accompanying Lectures}. Just like \href{https://solomonkurz.netlify.com/}{A Solomon Kurz} I appreciated all the work of McElreath but thought that this book and lecture series could be condensed into more of a primer length read.

To quote Kurz:

\begin{quote}
However, I prefer using Bürkner's \href{https://github.com/paul-buerkner/brms}{brms package} when doing Bayeian regression in R. \href{http://andrewgelman.com/2017/01/10/r-packages-interfacing-stan-brms/}{It's just spectacular}. I also prefer plotting with Wickham's \href{https://cran.r-project.org/web/packages/ggplot2/index.html}{ggplot2}, and coding with functions and principles from the \href{https://www.tidyverse.org}{tidyverse}, which you might learn about \href{http://style.tidyverse.org}{here} or \href{http://r4ds.had.co.nz/transform.html}{here}.
\end{quote}

So just like Kurz I am going to try and reimagine the \textbf{Second Edition} of this fabulous text in a more familiar/tidy format and greatly reduced in length!

\hypertarget{related-viewing}{%
\section*{Related viewing}\label{related-viewing}}
\addcontentsline{toc}{section}{Related viewing}

\href{https://www.youtube.com/watch?v=yakg94HyWdE}{This} is a very useful primer for anyone coming from a purely frequentist background (like me!).

\hypertarget{week-1-bayesian-inference-chapter-1-2-and-3}{%
\chapter{Week 1 \textbar{} Bayesian Inference \textbar{} Chapter 1, 2 and 3}\label{week-1-bayesian-inference-chapter-1-2-and-3}}

I will be covering this book week by week, and chapter by chapter, following along with the 2019 video lecture series (linked below). The associated \href{https://www.youtube.com/watch?v=4WVelCswXo4}{video} in his 2018/2019 video series covers this section. \href{https://speakerdeck.com/rmcelreath/l01-statistical-rethinking-winter-2019}{Slides} and \href{https://github.com/rmcelreath/statrethinking_winter2019/blob/master/homework/week01.pdf}{home work} for the first week!
For the second half of the week \href{https://speakerdeck.com/rmcelreath/l02-statistical-rethinking-winter-2019}{slides}, \href{https://www.youtube.com/watch?v=XoVtOAN0htU\&list=PLDcUM9US4XdNM4Edgs7weiyIguLSToZRI\&index=2}{video}.

\hypertarget{links-and-references}{%
\section*{Links and References}\label{links-and-references}}
\addcontentsline{toc}{section}{Links and References}

\href{https://xcelab.net/rm/statistical-rethinking/}{McElreath, R. (2016). \emph{Statistical rethinking: A Bayesian course with examples in R and Stan.} Chapman \& Hall/CRC Press.}

\hypertarget{link-to-solutions-for-homework}{%
\section*{Link to solutions for homework}\label{link-to-solutions-for-homework}}
\addcontentsline{toc}{section}{Link to solutions for homework}

Thank you Richard McElreayh for providing copies of all the above! \href{https://github.com/rmcelreath/statrethinking_winter2019/blob/master/homework/week01_solutions.pdf}{link}

\hypertarget{the-golem-of-prague}{%
\chapter{The Golem of Prague}\label{the-golem-of-prague}}

McElreath opens this book with a parabol of the golem of prague, and relates the mindless operation of a clay golem to the irresponsibly sourced models of todays research scientists and academics, to paraphrase:

\begin{quote}
Scientists also make golems \ldots{} These golems are scientific models. But these golems have real effects on the world, through the predictions they make and the intuitions they challenge or inspire. A concern with truth enlivens these models, but just like a golem or a modern robot, scientific models are neither true nor false, neither prophets nor charlatans. Rather they are constructs engineered for some purpose. These constructs are incredibly powerful, dutifully conducting their programmed calculations.
\end{quote}

This is a lot like the old adage \emph{All models are wrong, but some are useful} in that we as researchers can employ models all day long, but the wrong model, or the wrong golem will never be the correct tool and may lead to unexpected or unwanted answers.

\hypertarget{summary}{%
\section*{Summary}\label{summary}}
\addcontentsline{toc}{section}{Summary}

Chapter 1 of \textbf{Statistical Rethinking} contains no code but is definitly worth reading for a reminder of the unchecked power of statistical models for drawing inference that may have unforseen consequences.

\begin{quote}
Rather than idealized angels of reason, scientific models are powerful clay robots without intent of their own, bumbling along according to the myopic instructions they embody.
\end{quote}

Additionally, and of special interest is McElreaths rebuttal of Karl Poppers assertions on the necessary falsification of hypothesis. This is due to the following two points illustrating that all deductive falsification is nearly impossible for all scientific undertakings.

\begin{quote}
\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Hypotheses are not models. The relations among hypotheses and different kinds of
  models are complex. Many models correspond to the same hypothesis, and many
  hypotheses correspond to a single model. This makes strict falsification impossible.
\end{enumerate}
\end{quote}

\begin{quote}
\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{1}
\tightlist
\item
  Measurement matters. Even when we think the data falsify a model, another ob-
  server will debate our methods and measures. They don't trust the data. Sometimes they are right.
\end{enumerate}
\end{quote}

The scientific method, therefore, can not be reduced to a single statistical procedure, so we should not pretend our models are able to! This isn't to say scientific methods don't work, of course they do, and wonderfully, just not necessarily through falsification. Especially not falsificatin of a \textbf{NULL} hypothesis!

BDA may sounds confusing, but it is just glorified counting. Sum up all of the possible ways a thing can occur given some conditions, and all the the ways another thing can occur. The one with the higher ``count'' is of higher probability and more likely to occur. This is a natural extension of ordinary logic applied to continuous \emph{plausibility}. In this wasy BDA doesn't attempt to falsify a null hypothesis to prove a point instead it compares similar and \textbf{meaningful} models to infer plausibility. These will all be illustrated in the following chapter sections.

\hypertarget{small-worlds-and-large-worlds}{%
\chapter{Small Worlds and Large Worlds}\label{small-worlds-and-large-worlds}}

\href{https://www.youtube.com/watch?v=XoVtOAN0htU}{Video of this chapter} in lecture form can be found at that link, the slides to go along with this section \href{https://speakerdeck.com/rmcelreath/l02-statistical-rethinking-winter-2019}{here}. This chapter focuses on the \emph{Small World} part of the title, explaining probability in it's most basic and contained form, by counting. Bayesian inference is a natural extension of this logic, and this chapter will demonstrate how to build a model to learn from data and produce estimates. Next chapter we will deal with the \emph{Big World}.

\begin{quote}
The way that Bayesian models learn from evidence is arguably optimal in the small world. When their assumptions approximate reality, they also perform well in the large world. But large world performance has to be demonstrated rather than logically deduced. (p.~20)
\end{quote}

\hypertarget{the-garden-of-forking-data}{%
\section{The garden of forking data}\label{the-garden-of-forking-data}}

The garden of forking data is a story by Jorge Luis Borges that Richard cites as an example of counting possibilities. This offers an excellent example of the most fundamental engine of bayesian inference, counting. All bayesian inference, no matter how complex, is basically an exercise in this garden of forking data, and can be thought of as counting up all the ways data can happen, and choosing the most \textbf{probable} of the options.

\begin{quote}
A Bayesian analysis is a garden of forking data, in which alternative sequences of events
are cultivated. As we learn about what did happen, some of these alternative sequences are
pruned. In the end, what remains is only what is logically consistent with our knowledge.
This approach provides a quantitative ranking of hypotheses, a ranking that is maximally
conservative, given the assumptions and data that go into it. The approach cannot guarantee
a correct answer, on large world terms. But it can guarantee the best possible answer, on
small world terms, that could be derived from the information fed into it.
\end{quote}

\hypertarget{counting-possibilities.}{%
\subsection{Counting possibilities.}\label{counting-possibilities.}}

Assume we have a bag with four marbles in it that can either be white, 0, or blue, 1. There are then five possible combinations of 1's and 0's which could produce the contents of the bag, each of these possibilities is a \emph{conjecture}. The figure below illustrates all of the possible combinations of marbles.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{PossibleConjecture}
\end{Highlighting}
\end{Shaded}

\includegraphics{bookdown-demo_files/figure-latex/unnamed-chunk-5-1.pdf}

Our goal is to make our prediction on which of the above conjectures is the most \textbf{plausible} given some evidence from random draws from the bag. So we begin the exercise. We draw three marbles (with replacement) and they are Blue, White, Blue. We have our data, let's see if we can identify which conjecture is most likely. To illustrate this point, we will calculate the plausibility of conjecture 2 {[}Blue, White, White, White{]} being the true contents of the bag given our draws. To do this we will visualize the garden of branching data, then manually count up the possible ways each conjecture can manifest our draws, and compare.

Below you can see the evolution of this conjecture across the three draws. \textbf{A} shows the possible one blue and three white marbles upon the first draw, \textbf{B} shows the four possible second draws associated with each initial marble, and \textbf{C} shows the four possible marbles associated with second draw upon the third. When this is all draw up we can visually see the branches of the path that give us the draws Blue/White/Blue, and for this conjecture that is three possible branches, \textbf{D}.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{GardenPlots}
\end{Highlighting}
\end{Shaded}

\includegraphics{bookdown-demo_files/figure-latex/unnamed-chunk-8-1.pdf}

What remains to us is to tally up the possible ways the other conjectures can recreate the BWB selections, and then compare the plausibility of each model to determine the most likely conjecture based off this information. It should be obvious from the \emph{PossibleConjectures} figure that two of the possibile conjectures for the contents of the bag are blatantly impossible given the information we have. Both all white, or all blue marbles make drawing a bead of the opposite color impossible, eliminating them as possibilites. We will skip drawing out the first and second draw, and illustrate the possible branching paths that results from conjectures 2-4 combined in order make our first \textbf{inference} about the most likely contents of this bag.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{PossiblePaths}
\end{Highlighting}
\end{Shaded}

\includegraphics{bookdown-demo_files/figure-latex/unnamed-chunk-10-1.pdf}

From the above figure it's clear that there are different plausibilities for each conjecture, and if we were to rank them we could make a pretty good guess at what the most likely contents of the bag are. In this case, they are 9 {[}WBBB{]}, 8 {[}WWBB{]}, and 3 {[}WWWB{]}, which indicates that the most plausibile starting contents of this bag are, though not by much, three blue marbles and one white marble.

\hypertarget{bayesian-updating}{%
\subsection{Bayesian updating}\label{bayesian-updating}}

Let's say we make another draw from the bag, and it's another blue marble, what can we do with this new information? By multiplying the counts we already have for the conjecture, by the number of possible ways the new new draw can happen we can obtain an updated plausibility of each model.

\begin{longtable}[]{@{}llll@{}}
\toprule
Conjecture & Ways to draw one Blue & Prior Count & New Count\tabularnewline
\midrule
\endhead
{[}WWWWW{]} & 0 & 0 & 0\tabularnewline
{[}WWWB{]} & 1 & 3 & 3\tabularnewline
{[}WWBB{]} & 2 & 8 & 16\tabularnewline
{[}WBBB{]} & 3 & 9 & 27\tabularnewline
{[}BBBB{]} & 4 & 0 & 0\tabularnewline
\bottomrule
\end{longtable}

By updating our prior beliefs with new information, in this case from an additional draw, we can see that the likelyhood of each starting marble population drasticly changes and now the three blue, one white conjecture is significantly more likely than it appeared before. This type of updating doesn't have to be in the form of data in the same form, i.e.~it didn't have be an additional count. McElreath makes this example in his lecture series of learning new information about the relative rarity of each marble in the bag. In that case, the scarcity of each bag is a new multiplier on the plausibility of each conjecture, and by updating our model with new information, we update our view of the world. Lastly, it isn't always appropriate, or best, to work with count data like this. If ever. Transforming our data to probabilities by normalizing the data is a convientent way of doing this, and making the output comparable across models.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{ways <-}\StringTok{ }\KeywordTok{c}\NormalTok{( }\DecValTok{0}\NormalTok{ , }\DecValTok{3}\NormalTok{ , }\DecValTok{16}\NormalTok{ , }\DecValTok{27}\NormalTok{ , }\DecValTok{0}\NormalTok{ ) }\CommentTok{# The number of ways each conjecture can happen}
\NormalTok{ways}\OperatorTok{/}\KeywordTok{sum}\NormalTok{(ways) }\CommentTok{# The probability of each conjecture}
\end{Highlighting}
\end{Shaded}

\begin{verbatim}
## [1] 0.00000000 0.06521739 0.34782609 0.58695652 0.00000000
\end{verbatim}

\hypertarget{building-the-model}{%
\subsection{Building the model!}\label{building-the-model}}

We are going to use another toy example to illustrate how to build a bayesian model, and test an inference of an unknown parameter. This time we will use simulated data of observations of land or water on a tossed globe to estimated the ratio of land to water on the earth. This is a great visualization of the small world, large world model of statistics. Let's start with our data, an L is an observation of land, and a W is an observation of water.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{(}
\NormalTok{(Observations <-}\StringTok{ }\KeywordTok{tibble}\NormalTok{(}
  \DataTypeTok{obs =} \KeywordTok{c}\NormalTok{(}\StringTok{"w"}\NormalTok{, }\StringTok{"l"}\NormalTok{, }\StringTok{"w"}\NormalTok{, }\StringTok{"w"}\NormalTok{, }\StringTok{"w"}\NormalTok{, }\StringTok{"l"}\NormalTok{, }\StringTok{"w"}\NormalTok{, }\StringTok{"l"}\NormalTok{, }\StringTok{"w"}\NormalTok{)))}
\NormalTok{)}
\end{Highlighting}
\end{Shaded}

\begin{verbatim}
## # A tibble: 9 x 1
##   obs  
##   <chr>
## 1 w    
## 2 l    
## 3 w    
## 4 w    
## 5 w    
## 6 l    
## 7 w    
## 8 l    
## 9 w
\end{verbatim}

\hypertarget{a-data-story}{%
\subsubsection{A Data Story!}\label{a-data-story}}

It is important to consider storytelling when set up an experiment. This can help you identify parameters and convert an informal hypothesis into formal probability model

\begin{quote}
Bayesian data analysis usually means producing a story for how the data came to be. This story may be \emph{descriptive}, specifying associations that can be used to predict outcomes, given observations. Or it may be \emph{causal}, a theory of how come events produce other events.
\end{quote}

Important things to consider for the problem of water/land ratio on a simulated globe are as follows:

\begin{itemize}
\tightlist
\item
  The true proportion of water covering the globe is p.
\item
  A single toss of the globe has a probability p of producing a water (W) observation.
  It has a probability 1 − p of producing a land (L) observation.
\item
  Each toss of the globe is independent of the others.
\end{itemize}

These motivate and inform our data story, and can be translated into an actual statistical model for testing.

\hypertarget{bayesian-updating-1}{%
\subsubsection{Bayesian Updating}\label{bayesian-updating-1}}

We've seen one example of bayesian updating in theory earlier, by treating the known counts as a prior, and updating with new information, a fourth draw, or known rarities. Here we will illustrate the prior probability densities and the new, informed posterior probability distribution. Then we will treat the posterior as a new prior, update with a new observation, of land or sea, and repeat to ``converge'' on the best approximation of our parameter. The dotted line is the prior in each example, and the solid is the updated posterior distribution.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{BayesianUpdating}
\end{Highlighting}
\end{Shaded}

\includegraphics{bookdown-demo_files/figure-latex/unnamed-chunk-14-1.pdf}

\hypertarget{evaluate}{%
\subsubsection{Evaluate}\label{evaluate}}

To sum up what we have seen, I will quote McElreath:

\begin{quote}
The Bayesian model learns in a way that is demonstrably optimal, provided
that the real, large world is accurately described by the model. This is to say that your
Bayesian machine guarantees perfect inference, within the small world. No other way of
using the available information, and beginning with the same state of information, could do
better.
\end{quote}

But it is prudent to keep in mind that no model provides universal guarantees, and all models but be scrutinized and questioned.

\hypertarget{components-of-the-model}{%
\subsection{Components of the model}\label{components-of-the-model}}

We have seen model bayesian behaviour, it is now time to see the inner workings of these models so we can construct them ourselves. All models are made of \emph{variables}, these are usually things we wish to infer, like proportions and rates as in the previous examples, but also the data. In our previous example, we had three variables, two observed variables, the counts of water and land (W/L) and one unobserved variable, the proportion of these on the globe. Unobserved variables are usually what we will try to infer with our models and are known as \textbf{parameters}. Once we define all the variables in a model, we can build a model that \emph{relates} all the variables to one another, and infern the unobserved.

\textbf{Likelyhood} under the bayesian paradigm is slightly different in nature than under none bayesian statistics and will almost always refer not to a point estimate, but to a distribution funciton assigned to an \emph{observed} variable. For instance, in our example of the water/land observations, which has a binary result, the observed variables can be modeled with a binomial distribution, and a likelyhood can be calculated for and distribution.

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{dbinom}\NormalTok{( }\DecValTok{6}\NormalTok{ , }\DataTypeTok{size=}\DecValTok{9}\NormalTok{ , }\DataTypeTok{prob=}\FloatTok{0.5}\NormalTok{ )}
\end{Highlighting}
\end{Shaded}

\begin{verbatim}
## [1] 0.1640625
\end{verbatim}

That represents the relative number of ways we can get 6 draws from a possible 9 events given that there is an even probability of either event, such as a coin flip. This is represented by the red dot in the following diagram. As you can see, the binomial likely hood of this probability is not nearly the maximum, that value is closer to .66666, i.e., the proportion of the tosses.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{DbinomWL}
\end{Highlighting}
\end{Shaded}

\includegraphics{bookdown-demo_files/figure-latex/unnamed-chunk-17-1.pdf}

With all these bricks laid, we can now clearly define our model. The counts of water and land can be modeled as binomial distributions with paramter N and P (Count and Probability, where N is W+L).

\(B\sim Binomial(N,P)\)

The probability parameter P is further modeled as a uniform distribution:

\(P\sim Uniform(0,1)\)

This is known as a flat prior because the probability across its whole range is 1. This is a safe, but uninformative prior. We can now use these assumptions to create inferences.

\hypertarget{making-the-model-go}{%
\subsection{Making the model GO!}\label{making-the-model-go}}

When all the parameters of a model are defined, the prior distributions can be updated to the resulting \textbf{Posterior Distribution}.

\begin{quote}
This distribution contains the relative plausibility
of different parameter values, conditional on the data and model. The posterior distribution
takes the form of the probability of the parameters, conditional on the data.
\end{quote}

To illustrate the effect of the data and prior on the resultant posterior, we have the below diagram. McElreath once again best describes the effect we are seeing.

\begin{quote}
A flat prior constructs a posterior that is simply proportional to the likelihood. A step prior, assigning zero probability to all values less than 0.5, results in a truncated posterior. A peaked prior that shifts and skews the posterior, relative to the likelihood.
\end{quote}

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{PriorLikelhoodPosterior}
\end{Highlighting}
\end{Shaded}

\includegraphics{bookdown-demo_files/figure-latex/unnamed-chunk-19-1.pdf}

\hypertarget{motors}{%
\subsection{Motors}\label{motors}}

The last component of this recipe is the ``motor'' to make this whole model move. This is the function that processes the data and produces the posterior distribution. A useful practice is to think of this function as conditioning the prior on the data. We will discuss three such functions:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\tightlist
\item
  Grid approximation.
\item
  Quadratic approximation.
\item
  Markov Chain Monte Carlo (MCMC)
\end{enumerate}

\hypertarget{grid-approximation}{%
\subsubsection{Grid Approximation}\label{grid-approximation}}

Grid approximation is mostly unused except to teach. It will create a distribution by calculating the probability over N number of evenly spaced points in the probability space. In this way it can create nice, smooth posterior distributions, but is only really applicable for small, toy, problems with few parameters. As the parameter space increases GA becomes unfeasable and more elegant means for posterior creation are required. Below is another example of using GA for creating a posterior distribution.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{GridApproximation}
\end{Highlighting}
\end{Shaded}

\includegraphics{bookdown-demo_files/figure-latex/unnamed-chunk-21-1.pdf}

\hypertarget{quadratic-approximation}{%
\subsubsection{Quadratic Approximation}\label{quadratic-approximation}}

QA becomes appropriate quite quickly when models contain many parameters. It's easy when infering one paremter with a grid search of 100, but if that is a two parameter model that number becomes 100\^{}2, for three parameters 100\^{}3. For complicated multiparemter models this gets out of hand quickly. QA relies on the fact that posterior distributions near the maximum likely hood are well modeled by Gaussian Distribution,s and the log of a GD is a parabola which is a quadratic function, and this can be easily solved for. Computation is quick and inexpensive, especially when compared to GA and often times is an exact calculation of the maximum likelyhood, not an approximation at all! Quadratic approximations are generated as follows:

\begin{quote}
\begin{enumerate}
\def\labelenumi{(\arabic{enumi})}
\tightlist
\item
  Find the posterior mode. This is usually accomplished by some optimization algorithm, a procedure that virtually ``climbs'' the posterior distribution, as if it were a mountain. The golem doesn't know where the peak is, but it does know the slope under its feet. There are many well-developed optimization procedures, most of them more clever than simple hill climbing. But all of them try to find peaks.
\item
  Once you find the peak of the posterior, you must estimate the curvature near the peak. This curvature is sufficient to compute a quadratic approximation of the entire posterior distribution. In some cases, these calculations can be done analytically, but usually your computer uses some numerical technique instead.
\end{enumerate}
\end{quote}

To illustrate this point: Red is the QA and Black is the exact posterior distribution.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{QuadraticApproximation}
\end{Highlighting}
\end{Shaded}

\includegraphics{bookdown-demo_files/figure-latex/unnamed-chunk-23-1.pdf}

\hypertarget{mcmc}{%
\subsubsection{MCMC}\label{mcmc}}

MCMC performs where others can not, working in super high dimensionality and with large scale prior usage. It's development and adoption are largely responsible for the modern resurgence of Bayesian Inference.

\begin{quote}
The conceptual challenge with MCMC lies in its highly non-obvious strategy. Instead of
attempting to compute or approximate the posterior distribution directly, MCMC techniques
merely draw samples from the posterior. You end up with a collection of parameter values,
and the frequencies of these values correspond to the posterior plausibilities. You can then
build a picture of the posterior from the histogram of these samples.
\end{quote}

A working example using stan and our original data for the globe water prediction.

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{library}\NormalTok{(rstan)}
\KeywordTok{rstan_options}\NormalTok{(}\DataTypeTok{auto_write =} \OtherTok{TRUE}\NormalTok{)}
\KeywordTok{options}\NormalTok{(}\DataTypeTok{mc.cores =}\NormalTok{ parallel}\OperatorTok{::}\KeywordTok{detectCores}\NormalTok{())}

\NormalTok{W <-}\StringTok{ }\DecValTok{9}
\NormalTok{y <-}\StringTok{ }\KeywordTok{c}\NormalTok{(}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{,}\DecValTok{0}\NormalTok{,}\DecValTok{1}\NormalTok{)}
\end{Highlighting}
\end{Shaded}

Rstan connects R to Stan, a fast, compiled language that resembles C in syntax.

\begin{verbatim}
data {
 int W;
 int y[W];
}
parameters {
    real<lower=0,upper=1> theta;
}
model {
    theta ~ uniform(0,1);
    y ~ bernoulli(theta);
}
\end{verbatim}

Below you can see the output of our model estimating the parameter \emph{theta} which in this case is the proportion of water to land observe on a globe. MCMC runs four Markov Chains through the probability space, using No U-Turn Sampling (NUTS) to select the samples and build the Monte carlo approximation of the parameter in N-dimensional space. Much more on this later! For now be happy that MCMC and Stan have faithfully calculated our paremter, with associated uncertainties.

\begin{Shaded}
\begin{Highlighting}[]
\NormalTok{fit <-}\StringTok{ }\KeywordTok{sampling}\NormalTok{(Prop)}
\NormalTok{fit}
\end{Highlighting}
\end{Shaded}

\begin{verbatim}
## Inference for Stan model: d46d1fbcb5c7085fa06b4902c2f1ec8b.
## 4 chains, each with iter=2000; warmup=1000; thin=1; 
## post-warmup draws per chain=1000, total post-warmup draws=4000.
## 
##        mean se_mean   sd  2.5%   25%   50%   75% 97.5% n_eff Rhat
## theta  0.64    0.00 0.14  0.35  0.55  0.65  0.74  0.88  1819    1
## lp__  -7.75    0.02 0.78 -9.95 -7.93 -7.44 -7.26 -7.21  1633    1
## 
## Samples were drawn using NUTS(diag_e) at Thu May  2 16:39:47 2019.
## For each parameter, n_eff is a crude measure of effective sample size,
## and Rhat is the potential scale reduction factor on split chains (at 
## convergence, Rhat=1).
\end{verbatim}

To visualize this process, here are the four markov chains converging on our value of theta across the iterations.

\begin{Shaded}
\begin{Highlighting}[]
\KeywordTok{stan_trace}\NormalTok{(fit)}
\end{Highlighting}
\end{Shaded}

\includegraphics{bookdown-demo_files/figure-latex/unnamed-chunk-27-1.pdf}

This concludes the first week of Statistical Rethinking!


\end{document}
