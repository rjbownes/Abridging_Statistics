--- 
title: "*Statistical Rethinking* | Notes and Examples"
subtitle: "Learning from Failure"
author: ["Richard Bownes"]
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
output: bookdown::gitbook
documentclass: book
link-citations: yes
github-repo: rjbownes/The.Doctorate.Philosophy
twitter-handle: rjbownes
description: "I am going to document refreshing statistics and bayesian analysis in a through this git book format for myself and others."
---

# Statistical Abridging {-}

Richard McElreath’s [*Statistical Rethinking* text](http://xcelab.net/rm/statistical-rethinking/) is the entry-level textbook for applied researchers. [Accompanying Lectures](https://www.youtube.com/channel/UCNJK6_DZvcMqNSzQdEkzvzA/playlists). Just like [A Solomon Kurz](https://solomonkurz.netlify.com/) I appreciated all the work of McElreath but thought that this book and lecture series could be condensed into more of a primer length read. 

To quote Kurz:

>However, I prefer using Bürkner’s [brms package](https://github.com/paul-buerkner/brms) when doing Bayeian regression in R. [It's just spectacular](http://andrewgelman.com/2017/01/10/r-packages-interfacing-stan-brms/). I also prefer plotting with Wickham's [ggplot2](https://cran.r-project.org/web/packages/ggplot2/index.html), and coding with functions and principles from the [tidyverse](https://www.tidyverse.org), which you might learn about [here](http://style.tidyverse.org) or [here](http://r4ds.had.co.nz/transform.html).

So just like Kurz I am going to try and reimagine the **Second Edition** of this fabulous text in a more familiar/tidy format and greatly reduced in length!

## Related viewing {-}

[This](https://www.youtube.com/watch?v=yakg94HyWdE) is a very useful primer for anyone coming from a purely frequentist background (like me!).

```{r, echo = F, message = F, warning = F, results = "hide"}
pacman::p_unload(pacman::p_loaded(), character.only = TRUE)
```
